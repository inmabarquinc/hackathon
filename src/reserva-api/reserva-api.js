import { LitElement, html } from "lit-element";

class ReservaApi extends LitElement {
    
     
    static get properties(){
        return{
                reservas: {type: Array},
                cargar:  {type: Boolean}
                
        };
    }

    constructor(){
        //la ejecución se ejecuta aqui
        super();

        //inicializamos vacio
        this.reservas = [];
 
        this.getReservaData();
        console.log("constructor de reserva-api");
/*
      this.reservas = [
        {
            idReserva: 1,
            fechaReserva: "2021-10-30",
            horaReserva: "21:00",
            numComensales: 4,
            comentariosAd: "Tenemos un celíaco",
            nombreReserva: "Roberta"
        },
        {
            idReserva: 2,
            fechaReserva: "2021-10-30",
            horaReserva: "21:00",
            numComensales: 7,
            comentariosAd: "segundo",
            nombreReserva: "segunadarese"
        }
    ]
*/
    }

    getReservaData(){
        console.log("estoy en getReservaData");
        console.log("Obteniendo datos de la reserva");

        //Variable en js -- let , var
        let xhr = new XMLHttpRequest();
        

        console.log(xhr);
        //el onload se llama cuando acaba el open/send
        //se ejecuta cuando recibe el callback, realmente el onload=callback
        
        xhr.onload = () => {
            //aqui trabajamos con el resultado de la respuesta
            if(xhr.status == 200){
                //200 ok
                console.log("Peticion completada correctamente 200");

                
                console.log(JSON.parse(xhr.responseText));
                let APIResponse =JSON.parse(xhr.responseText);
               // let APIResponse =xhr.responseText;

                //no asumir que la API tiene un campo RESULTS de salida
                //hacer el console log para ver qué está saliendo de la API
                console.log("consle log de APIResponse:" + APIResponse);
                //this.reservas=APIResponse.results;
                this.reservas = APIResponse;
                console.log("this reservas -->>>>" + this.reservas);

            }
        }
        
        //open no envia la peticion
        //abrimos el canal
        xhr.open("GET", "http://localhost:8080/restaurante/reservas/");
        //envia la peticion, si hay q enviar un body sería: xhr.send(body);
        xhr.send();

        console.log("Fin de getReservaData");

    }


        //ciclo de vida de litelement
        updated(changedProperties){
            console.log("updated de reserva-api");
            if (changedProperties.has("reservas")){
                console.log("ha cambiado el valor de la propiedad reservas en reserva-api");
    
                this.dispatchEvent(
                    new CustomEvent(
                            "reservas-data-updated",
                    {
                    detail:{
                                reservas: this.reservas
                           
                        }
                    }
                    )  
                )
            }

          




        }




}

customElements.define('reserva-api', ReservaApi);