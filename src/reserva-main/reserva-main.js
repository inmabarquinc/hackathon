import { LitElement, html } from "lit-element";
import '../reserva-ficha-listado/reserva-ficha-listado.js'
import '../reserva-api/reserva-api.js'
import '../reserva-form/reserva-form.js'

import '../reserva-api-eliminar/reserva-api-eliminar.js'
import '../reserva-api-add/reserva-api-add.js'


class ReservaMain extends LitElement {
    
     
    static get properties(){
        return{
            reservas: {type: Array},
            verFormularioReserva: {type: Boolean}
        };
    }

    constructor(){
        super();
        this.reservas =[];
        //por defecto ponemos false
        this.verFormularioReserva = false;
    }

    render (){
        return html`
       
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h2 class="text-center">Reservas</h2> 
            <div class="row" id="reservaList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.reservas.map(
                        ocurreser => html`<reserva-ficha-listado 
                            idReserva="${ocurreser.idReserva}"
                            fechaReserva="${ocurreser.fechaReserva}"
                            horaReserva="${ocurreser.horaReserva}"
                            numComensales="${ocurreser.numComensales}"
                            comentariosAd="${ocurreser.comentariosAd}"
                            nombreReserva="${ocurreser.nombreReserva}"
                            @eliminar-reserva="${this.eliminarReserva}"
                            @modificar-reserva="${this.modificarReserva}" 
                            ></reserva-ficha-listado>`
                    )}
                </div>
            </div>   

            <div class= "row">
                <reserva-form 
                    @reserva-form-close="${this.reservaFormClose}" 
                    @reserva-form-store="${this.reservaFormStore}" 
                    class="d-none border rounded border-primary"
                    id="reservaForm"> 
                </reserva-form>
            </div>


            <div class= "row">
                <reserva-api @reservas-data-updated = "${this.reservasDataUpdated}"></reserva-api>
                <reserva-api-eliminar></reserva-api-eliminar>
                <reserva-api-add></reserva-api-add>
                
            </div>

    `; 
    }

    updated(changedProperties){
        //ciclo de vida del litelement se ejecuta cuando cambia de valor de una propiedad del componente
        console.log("entramos a updated - reserva-main");
        if(changedProperties.has("verFormularioReserva")){
            console.log("Ha cambiado el valor de la propiedad verFormularioReserva en persona-main");
            if(this.verFormularioReserva === true){
                console.log("verFormularioReserva === true");
                this.verFormularioReservaData();
            }else{
                console.log("verFormularioReserva === false");
                this.verListaReserva();
            }
        }

    }

    verFormularioReservaData(){
        console.log("Entro en verFormularioReservaData");
        console.log("Mostrar formulario de reserva");
        this.shadowRoot.getElementById("reservaForm").classList.remove("d-none");
        this.shadowRoot.getElementById("reservaList").classList.add("d-none");
    }

    verListaReserva(){
        console.log("Entro en verListaReserva");
        console.log("Mostrar listado de reservas");
        this.shadowRoot.getElementById("reservaForm").classList.add("d-none");
        this.shadowRoot.getElementById("reservaList").classList.remove("d-none");
        
    }



    eliminarReserva(e){
        console.log("estamos en eliminarReserva de reserva-main");
        console.log("Vamos a eliminar a la reserva: " + e.detail.idReserva);
        this.shadowRoot.querySelector("reserva-api-eliminar").accionDelete = true;
        this.shadowRoot.querySelector("reserva-api-eliminar").idReserva = e.detail.idReserva;
        this.shadowRoot.querySelector("reserva-api-eliminar").fechaReserva = e.detail.fechaReserva;
        this.shadowRoot.querySelector("reserva-api").cargar = true;
    }

    reservasDataUpdated(e){
        console.log("*************************************************estamos en reservasDataUpdated en reserva-main");
        console.log(e.detail);
        this.reservas = e.detail.reservas;
    }

    reservaFormClose(){
        console.log("reservaFormClose");
        console.log("Se ha cerrado el formulario de la reserva");
        this.verFormularioReserva = false;
    }



    modificarReserva(e){
        console.log("modificarReserva en reserva-main");
        console.log(e);
        console.log("Se ha pedido mas información de la reserva" + e.detail.idReserva);
        //this.people es el array
         
//llamamos a la api de consulta con el valor de la idreserva

        let chosenReserva=this.reservas.filter(
            reserva=> reserva.idReserva===e.detail.idReserva
        );
        console.log(chosenReserva);
        //si queremos acceder al idReserva seria asi:
        //console.log(chosenPerson[0].idReserva);
 
        let reserva={}
        reserva.idReserva=chosenReserva[0].idReserva;
        reserva.nombreReserva=chosenReserva[0].nombreReserva;
        reserva.fechaReserva=chosenReserva[0].fechaReserva;
        reserva.horaReserva=chosenReserva[0].horaReserva;
        reserva.numComensales=chosenReserva[0].numComensales;
        reserva.comentariosAd=chosenReserva[0].comentariosAd;

         console.log(chosenReserva[0].idReserva);
         console.log(chosenReserva[0].nombreReserva);
         console.log(chosenReserva[0].fechaReserva);
         console.log(chosenReserva[0].horaReserva);
         console.log(chosenReserva[0].numComensales);
         console.log(chosenReserva[0].comentariosAd);
 
         //pasamos los valores al formulario

         

        //pasamos los valores al formulario

        this.shadowRoot.getElementById("reservaForm").reserva=reserva;


        //para q aparezca deshabilitado estamos editando.
        this.shadowRoot.getElementById("reservaForm").editingReserva=true;
        this.verFormularioReserva= true;


    }
    reservaFormStore(e){
        console.log("reservaFormStore en reserva-main");
        console.log("reservaFormStore Se ha pulsado en Guardar en formulario de la reserva");
        //tenemos que distinguir si estamos añadiendo o actualizando
        if(e.detail.editingReserva === true){
            //UPDATE

            console.log("se va actualizar una reserva");


              // this.reserva=  this.reserva.map(
              //  reserva => reserva.name === e.detail.reserva.idReserva 
              //           ? reserva = e.detail.reserva : reserva);
                //si se cumple la condicion --> reserva.name === e.detail.reserva.idReserva  
                //si es true se hace e.detail.reserva
                //si es false es reserva , no se modifica. Se deja asi.

                console.log("reservaFormStore - La propiedad idReserva vale "+ e.detail.reserva.idReserva);
                console.log("reservaFormStore - La propiedad nombreReserva vale "+e.detail.reserva.nombreReserva);
                console.log("reservaFormStore - La propiedad fechaReserva vale "+e.detail.reserva.fechaReserva);
                console.log("reservaFormStore - La propiedad horaReserva vale "+e.detail.reserva.horaReserva);
                console.log("reservaFormStore - La propiedad numComensales vale "+e.detail.reserva.numComensales);
                console.log("reservaFormStore - La propiedad comentariosAd vale "+e.detail.reserva.comentariosAd);

        }else{
                //ADD
                console.log("se va añadir una nueva reserva - estamos en reserva-main")
                console.log("reservaFormStore - La propiedad idReserva vale "+ e.detail.reserva.idReserva);
                console.log("reservaFormStore - La propiedad nombreReserva vale "+e.detail.reserva.nombreReserva);
                console.log("reservaFormStore - La propiedad fechaReserva vale "+e.detail.reserva.fechaReserva);
                console.log("reservaFormStore - La propiedad horaReserva vale "+e.detail.reserva.horaReserva);
                console.log("reservaFormStore - La propiedad numComensales vale "+e.detail.reserva.numComensales);
                console.log("reservaFormStore - La propiedad comentariosAd vale "+e.detail.reserva.comentariosAd);


                this.shadowRoot.querySelector("reserva-api-add").accionAdd = true;
                this.shadowRoot.querySelector("reserva-api-add").reserva = e.detail.reserva;





            // js spread syntax
           // this.reservas= [...this.reservas, e.detail.reserva];

        } 

        console.log(e.detail.reserva);
        this.verFormularioReserva = false;      

    }




}

customElements.define('reserva-main', ReservaMain);
