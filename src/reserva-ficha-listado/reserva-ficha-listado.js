import { LitElement, html } from "lit-element";


class ReservaFichaListado extends LitElement {
    
     
    static get properties(){

        return{
            idReserva:  {type: Number},
            fechaReserva:  {type: String},
            horaReserva:  {type: String},
            numComensales:  {type: Number},
            comentariosAd:  {type: String},
            nombreReserva:  {type: String}
        };
    }

    constructor(){
        super();

    }

    render (){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div class="card h-100">
            
            <div class="card-body">
                <h5 class="card-title">Id Reserva: ${this.idReserva}</h5>
                <h5 class="card-title">Nombre Reserva: ${this.nombreReserva}</h5>
                <p class="card-text">Fecha reserva: ${this.fechaReserva}</p>
                <p class="card-text">Hora reserva: ${this.horaReserva}</p>
                <p class="card-text">Número de comensales: ${this.numComensales}</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Comentarios: ${this.comentariosAd}</li>
                </ul>

                <div class="card-footer">
                    <button @click="${this.eliminarReserva}"class="btn btn-danger col-5"><strong>Eliminar</strong></button>
                    <button @click="${this.modificarReserva}"class="btn btn-danger col-5"><strong>Modificar</strong></button>
                  
                </div>



            </div>
            
        </div>
    `; 
    }

     
    modificarReserva(){
        console.log("entramos en modificarReserva de reserva-ficha-listado");
        console.log("se va a modificar la reserva con id: "+this.idReserva);

        this. dispatchEvent(
            new CustomEvent(
                "modificar-reserva",
                {
                    "detail" : {
                        idReserva: this.idReserva
                    }
                }
            )
        )
    }

    eliminarReserva(){
        console.log("entramos en eliminarReserva de reserva-ficha-listado");
        console.log("se va a eliminar la reserva con id: "+ this.idReserva);

        this. dispatchEvent(
            new CustomEvent(
                "eliminar-reserva",
                {
                    "detail" : {
                        idReserva: this.idReserva
                    }
                }
            )
        )


    }
   
}

customElements.define('reserva-ficha-listado', ReservaFichaListado); 