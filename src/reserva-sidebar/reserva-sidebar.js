import { LitElement, html } from "lit-element";

class ReservaSidebar extends LitElement {
    
     
    static get properties(){
        return{

            idReserva:  {type: Number},


        };
    }

    constructor(){
        super();
       
    }

    render (){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <aside>
            <section>
                <div class="mt-5">
                    <button @click="${this.newReserva}" class="w-100 btn btn-success" style="font-size: 25px"><strong>Nueva reserva</strong></button>
                    </>
                </div>
                <div class="mt-5">
                    <div class="form-group">
                        <label>Id reserva</label>
                        <input     
                                type="text" 
                                class="form-control" 
                                placeholder= "número de teléfono"
                                .value="${this.idReserva}"
                        </input>
                    </div>
                    <button @click="${this.newbusqueda}" class="w-100 btn btn-success" style="font-size: 25px"><strong>Buscar</strong></button>
                    </>
                </div>
            </section>
        </aside>

    `; 
    }

    newReserva(){
        console.log("Entramos en newReserva");
        console.log("Se va a crear una nueva reserva - en reserva-sidebar");

        this.dispatchEvent(new CustomEvent ("new-reserva", {}));
        //Donde se va a recibir el evento no necesita información, por so no le envia nada {}
    }



    newbusqueda(){
        console.log("Entramos en newbusqueda");
        console.log("Se va a realizar la búsqueda por ID");

        this. dispatchEvent(
            new CustomEvent(
                "new-busqueda",
                {
                    "detail" : {
                        idReserva: this.idReserva
                    }
                }
            )
        )

    }


}

customElements.define('reserva-sidebar', ReservaSidebar);