import { LitElement, html } from "lit-element";

class ReservaApiEliminar extends LitElement {
    
    static get properties(){
        return{
            idReserva: {type: String},
            fechaReserva: {type: String},
            accionDelete: {type: Boolean}
        };
    }

    constructor(){
        //la ejecución se ejecuta aqui
        super();

        //inicializamos vacio
        this.idReserva = "";
        this.fechaReserva = "";
        //this.accionDelete = false;
 
        //this.deleteReservaData();

    }

    deleteReservaData(){

        console.log("Estamos en deleteReservaData");
        let xhr = new XMLHttpRequest();
        console.log(xhr);
        //el onload se llama cuando acaba el open/send
        //se ejecuta cuando recibe el callback, realmente el onload=callback  
        xhr.onload = () => {
            //aqui trabajamos con el resultado de la respuesta
            if(xhr.status == 200){
                //200 ok
                console.log("Peticion completada correctamente 200");
               
            }
        }
        
        //
       
        console.log("this.idReserva -->>>>" + this.idReserva);

        //open no envia la peticion
        //abrimos el canal
        xhr.open("DELETE", "http://localhost:8080/restaurante/reservas/"+this.idReserva);
        xhr.setRequestHeader("Content-Type","application/json");
        
    
       
        xhr.send();
        console.log("Fin de deleteReservaData");
    }

        //ciclo de vida de litelement
        updated(changedProperties){
            console.log("updated - reserva-api-eliminar");
            if(changedProperties.has("accionDelete")){
                console.log("updated - accionDelete");
                console.log("this.idReserva="+this.idReserva);
                console.log("this.fechaReserva="+this.fechaReserva);
                console.log("this.accionDelete="+this.accionDelete);
                this.deleteReservaData();
            }
        }

}

customElements.define('reserva-api-eliminar', ReservaApiEliminar);