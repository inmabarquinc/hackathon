import { LitElement, html } from "lit-element";
import '../reserva-main/reserva-main.js'
import '../reserva-sidebar/reserva-sidebar.js'
import '../reserva-api/reserva-api.js'


class ReservaApp extends LitElement {
    
     
    static get properties(){
        return{

        };
    }

    constructor(){
        super();
    }

    render (){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div class="row">
        
            <reserva-sidebar 
                @new-reserva="${this.newReserva}" class="col-2">
                @new-busqueda="${this.newBusqueda}" class="col-2">
                </reserva-sidebar>
            <reserva-main class="col-9"></reserva-main>
        </div>
    `; 
    }

    newReserva(e){
        console.log("Estamos en newReserva de reserva-app");
        this.shadowRoot.querySelector("reserva-main").verFormularioReserva= true;  
    }

    newBusqueda(e){
        console.log("Estamos en newBusqueda de reserva-app");
        this.shadowRoot.querySelector("reserva-api").idReserva= e.detail.idReserva;  
    }
}

customElements.define('reserva-app', ReservaApp);