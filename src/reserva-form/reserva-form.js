import { LitElement, html } from "lit-element";

class ReservaForm extends LitElement {
    
     
    static get properties(){
        return{
            //vamos a guardar toda la info que se incluya en el formualrio en in object
            reserva: {type: Object},
            //booleamo para manejar si estamos  añadiendo persona o actualziando
            editingReserva: {type: Boolean}

        };
    }

    constructor(){
        super();
        //inicializamos 
       // this.person={};

        //Para q en la carga no aparezca el valor undefined, debemos de poner valores por defecto
       //comentamos el //this.person={}; xq lo hemos incluido en esta nueva funcion 
        this.resetFormData();

    }

    render (){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div>
            <form>
                <div class="form-group">
                    <label>Id reserva</label>
                    <input  @input="${this.updateIdReserva}"   
                            type="text" 
                            class="form-control" 
                            placeholder= "número de teléfono"
                            .value="${this.reserva.idReserva}"
                            ?disabled="${this.editingReserva}"
                    </input>
                </div>

                <div class="form-group">
                    <label>Nombre Reserva</label>
                    <input @input ="${this.updateNombreReserva}" 
                                .value="${this.reserva.nombreReserva}"
                                class="form-control" 
                                placeholder= "Nombre reserva"      
                    </input>
                </div> 
                
                <div class="form-group">
                    <label>Fecha reserva</label>
                    <input @input="${this.updateFechaReserva}" 
                            type="text" 
                            class="form-control" 
                            placeholder= "Fecha reserva"
                            .value="${this.reserva.fechaReserva}"
                    </input>
                </div>

                <div class="form-group">
                    <label>Hora reserva</label>
                    <input @input="${this.updateHoraReserva}" 
                            type="text" 
                            class="form-control" 
                            placeholder= "Hora reserva"
                            .value="${this.reserva.horaReserva}"
                    </input>
                </div>

                <div class="form-group">
                    <label>Número comensales</label>
                    <input @input="${this.updateNumComensales}" 
                            type="text" 
                            class="form-control" 
                            placeholder= "Número comensales"
                            .value="${this.reserva.numComensales}"
                    </input>
                </div>

                <div class="form-group">
                    <label>Comentarios</label>
                    <textarea @input="${this.updateComentarios}" 
                        type="text" 
                        class="form-control" 
                        placeholder= "Comentarios"
                        .value="${this.reserva.comentariosAd}"
                        rows="4">
                     </textarea>
            </div>

                <button @click="${this.goBack}"  class="btn btn-primary"><strong>Atrás</strong></button>
                <button @click="${this.guardarReserva}" class="btn btn-success"><strong>Guardar</strong></button>


            </form>
        </div>

    `; 
    }

    guardarReserva(e){
        console.log("guardarReserva"); 
        e.preventDefault();
        console.log("reserva-form - La propiedad idReserva vale "+ this.reserva.idReserva);
        console.log("reserva-form - La propiedad nombreReserva vale "+this.reserva.nombreReserva);
        console.log("reserva-form - La propiedad fechaReserva vale "+this.reserva.fechaReserva);
        console.log("reserva-form - La propiedad horaReserva vale "+this.reserva.horaReserva);
        console.log("reserva-form - La propiedad numComensales vale "+this.reserva.numComensales);
        console.log("reserva-form - La propiedad comentariosAd vale "+this.reserva.comentariosAd);

     
        this.dispatchEvent(
            new CustomEvent(
                    "reserva-form-store",
            {
            detail:{
                    reserva: {
                        idReserva: this.reserva.idReserva,
                        nombreReserva: this.reserva.nombreReserva,
                        fechaReserva: this.reserva.fechaReserva,
                        horaReserva: this.reserva.horaReserva,
                        numComensales: this.reserva.numComensales,
                        comentariosAd: this.reserva.comentariosAd,
                    },
                    editingReserva: this.editingReserva
                }
            }
            )  
        )
        this.resetFormData();
    }


    updateIdReserva(e){
       console.log("actualizando el valor idReserva"); 
      // console.log("actualizando el valor name con el valor " + e.target.value);
       this.reserva.idReserva= e.target.value;
    }
    updateNombreReserva(e){
        console.log("actualizando el valor nombreReserva"); 
        this.reserva.nombreReserva= e.target.value;
    }
    updateFechaReserva(e){
        console.log("actualizando el valor updateNombreRefechaReservaserva"); 
        this.reserva.fechaReserva= e.target.value;
    }
    updateHoraReserva(e){
        console.log("actualizando el valor horaReserva"); 
        this.reserva.horaReserva= e.target.value;
    }
    updateNumComensales(e){
        console.log("actualizando el valor numeroComensales"); 
        this.reserva.numComensales= e.target.value;
    }
    updateComentarios(e){
        console.log("actualizando el valor comentariosAd"); 
        this.reserva.comentariosAd= e.target.value;
    }


    goBack(e){
        //cómo hacer que no se haga el submit
        console.log("entramos en goBack");
        e.preventDefault();

        //se deben inicializar los values de los text area para q no quede el valor que hemos consultado
        this.resetFormData();

        this.dispatchEvent(new CustomEvent("reserva-form-close", {}));

    }

    resetFormData(){
        console.log("Entramos en resetFormData() desde reserva-form");

        //reinicializamos el objeto
        this.reserva={}
        this.reserva.idReserva= "";
        this.reserva.fechaReserva= "";
        this.reserva.horaReserva= "";
        this.reserva.numComensales= "";
        this.reserva.comentariosAd= "";
        this.reserva.nombreReserva= "";
        

         //reseteamos la variable para que vuelva a false
         //no estamos editando, y por lo tanto lo resetamos
        this.editingReserva =false;
    }
}

customElements.define('reserva-form', ReservaForm);

